#!/usr/bin/env pythoh3



import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []
    for i in range(min_trees,
                   max_trees + 1):
        productions.append((i, compute_trees(i)))
    return productions

def read_arguments():
    if len(sys.argv) != 6:
        sys.exit("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])
    except ValueError:
        sys.exit("All arguments must be integers.")

    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees, fruit_per_tree, reduction
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    production_maxima = 0
    trees = 0

    for i in range(0, len(productions)):
        print(productions[i][0], productions[i][1])
        if productions[i][1] > production_maxima:
            production_maxima = productions[i][1]
            trees = productions[i][0]


    print(f"Best production: {production_maxima}, for {trees} trees")


if __name__ == '__main__':
    main()
